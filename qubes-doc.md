# Hypervisor rocks!
![Screenshot](https://github.com/c4tzz/qubes-os/blob/master/screenshots/qubes-os.png)


# Templates Fix

Fix `Archlinux Template` network, add to `/etc/pacman.d/01-qubes-proxy.conf`

```XferCommand = /usr/bin/env ALL_PROXY=http://127.0.0.1:8082/ /usr/bin/curl -C - -f -o %o %u```

But better replace 126 line with `/usr/lib/qubes/update-proxy-configs`


Fresh build, cannot open any window

https://github.com/QubesOS/qubes-issues/issues/5442

Fix `Archlinux Template`, in `dom0` `sudo xl console archlinux` 

`mkfs.ext4 /dev/xvdb`

# Benchmark templates start/shutdown time
```
14s - start
5s - shut
```

# Split Wireshark

```
#!/bin/bash
# safety settings
set -u
set -e
set -o pipefail

# Assumes that both this VM and the target VM have added the default user to "wireshark" (or similar) group, so the user has enough permission to run dumpcap.
dumpcap -P -w - "$@" | qvm-run \$dispvm 'wireshark -k -i -' 
```

# Dom0 packages for wm managers
```
sudo qubes-dom0-update gcc-c++ \
      cairo-devel cmake automake xcb-util-devel libxcb-devel xcb-proto dbus-devel \
      xcb-util-image-devel i3-ipc wireless-tools-devel libnl3-devel xcb-util-wm-devel \
      flex bison libxkbcommon-devel libxkbcommon-x11-devel pango-devel startup-notification-devel librsvg2-devel \
      libXcomposite-devel libXrandr-devel libXinerama-devel libconfig-devel asciidoc imlib2-devel
```
# Bios Flashing

`https://github.com/osresearch/heads`

`https://www.coreboot.org/Board:asus/kgpe-d16`

`https://git.lsd.cat/g/thinkpad-coreboot-qubes`

# Hardened kernel

`https://www.whonix.org/wiki/Linux_Kernel_Runtime_Guard_LKRG#Installation`

`https://github.com/coldhakca/coldkernel`

`https://github.com/tasket/Qubes-VM-hardening`

# Firewall alternative

`https://github.com/mirage/qubes-mirage-firewall`

# Qubes OS testing iso builds

`https://openqa.qubes-os.org/`

# Useful Qubes Repositories

archlinux

`https://neowutran.ovh/qubes/vm-archlinux/`

voidlinux

`https://github.com/Nexolight/void-tainted-pkgs/tree/qubes/QubesOS`

cruxlinux

`https://github.com/hexparrot/qubes-crux`

gentoolinux

`https://github.com/aidecoe/qubes-builder-gentoo`

ubuntulinux

`https://qubes.3isec.org/Templates_4.0/`

kalilinux

`https://github.com/fepitre/qubes-template-kali`

nixos

`https://github.com/jmitchell/qubes-builder-nixos`

windows

`https://github.com/crazyqube/qvm-create-windows-qube`

windows-gaming

`https://github.com/Qubes-Community/Contents/blob/master/docs/customization/windows-gaming-hvm.md`

# Android-x86 7.1-r2 with GAPPS

`https://groups.google.com/forum/#!searchin/qubes-users/android%7Csort:date/qubes-users/HGAT6DmuQkM/9lRBBAVmDAAJ`

# Hackintosh Template (2015)

```
https://groups.google.com/forum/#!msg/qubes-users/RiVntUzgJmY/rXMtXD3WKQAJ
```

# Crux Networking

```
export IP=10.137.0.x   [get x from Qube Settings:IP]
export GW=10.137.0.y   [get y from Qube Settings:Gateway]
export DEV=eth0

ifconfig $DEV up
ip route add $GW dev $DEV
ip route add default via $GW
ip addr add $IP/32 dev $DEV
ip link set $DEV up

echo 'nameserver 10.139.1.1' >> /etc/resolv.conf
```
# Copy from dom0 to an appvm
```
#/bin/sh
#
# usage ./cp-domain <vm_name> <file_to_copy>
#
domain=$1
file=$2
fname=`basename $file`

qvm-run $domain 'mkdir /home/user/incoming/dom0 -p'
cat $file| qvm-run --pass-io $domain "cat > /home/user/incoming/dom0/$fname"
```
# Make Builder AppVM from Dom0

```
#!/usr/bin/env bash
 
VM_NAME=builder

qvm-create --class AppVM --template fedora-30 --label blue --property virt_mode=pvh $VM_NAME
qvm-volume extend $VM_NAME:private 30g
qvm-run --auto --user user --pass-io $VM_NAME 'sudo dnf -y install git \
	createrepo squashfs-tools rpm-build make wget rpmdevtools dialog devscripts mlocate \
	rpm-sign gnupg dpkg-dev debootstrap python2-sh perl-Digest-MD5 \
       	perl-Digest-SHA'
qvm-run --auto --user user --pass-io $VM_NAME 'curl https://keys.qubes-os.org/keys/qubes-master-signing-key.asc | gpg --import'
qvm-run --auto --user user --pass-io $VM_NAME 'curl https://keys.qubes-os.org/keys/qubes-developers-keys.asc | gpg --import'
qvm-run --auto --user user --pass-io $VM_NAME 'export GIT_PREFIX="https://github.com/QubesOS/qubes-builder.git" && export DIR=qubes-builder && git clone $GIT_PREFIX'
```
# GuixSD in HVM

Set up Standalone HVM with `kernel ""`

Allocate memory

Check allocated IP address : `qvm-ls -n`

Boot:

`qvm-start HVM --cdrom=iso:/home/user/iso/guixsd.iso`

At root prompt:

```

ifconfig -a

ifconfig eth0 10.137.0.16 

ifconfig eth0 up

route add default gw 10.137.0.6

echo nameserver 9.9.9.9 >> /etc/resolv.conf

```

Disks:

`cfdisk /dev/xvda`

Configure disks as required: simplest case would be dos, with one partition in `/dev/xvda1`

No swap

```
mkfs.ext4 -L root /dev/xvda1
mount LABEL=root /mnt
herd start cow-store /mnt
mkdir /mnt/etc
cp /etc/configuration/desktop.scm /mnt/etc/config.scm`
```

Edit `/mnt/etc/config.scm` to match requirements:

In particular, check `LABEL`

`guix system init /mnt/etc/config.scm /mnt`

Watch the system download package and install in `/mnt`

Reboot
